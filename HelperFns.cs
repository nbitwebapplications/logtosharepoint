﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LogToSharePoint
{
    public static class HelperFns
    {
        // https://stackoverflow.com/a/50819828
        public static string ReadLastLine(string path)
        {
            // open read only, we don't want any chance of writing data
            using (System.IO.Stream fs = System.IO.File.OpenRead(path))
            {
                // check for empty file
                if (fs.Length == 0)
                {
                    return null;
                }

                // start at end of file
                fs.Position = fs.Length - 1;

                // the file must end with a '\n' char, if not a partial line write is in progress
                int byteFromFile = fs.ReadByte();
                if (byteFromFile != '\n')
                {
                    // partial line write in progress, do not return the line yet
                    return null;
                }

                // move back to the new line byte - the loop will decrement position again to get to the byte before it
                fs.Position--;

                // while we have not yet reached start of file, read bytes backwards until '\n' byte is hit
                while (fs.Position > 0)
                {
                    fs.Position--;
                    byteFromFile = fs.ReadByte();
                    if (byteFromFile < 0)
                    {
                        // the only way this should happen is if someone truncates the file out from underneath us while we are reading backwards
                        throw new System.IO.IOException("Error reading from file at " + path);
                    }
                    else if (byteFromFile == '\n')
                    {
                        // we found the new line, break out, fs.Position is one after the '\n' char
                        break;
                    }
                    fs.Position--;
                }

                // fs.Position will be right after the '\n' char or position 0 if no '\n' char
                byte[] bytes = new System.IO.BinaryReader(fs).ReadBytes((int)(fs.Length - fs.Position));
                return System.Text.Encoding.UTF8.GetString(bytes);
            }
        }
        public static string GetProcessName(string line)
        {
            var rexcode = new Regex(@"(?>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2},\d+\s\[\d+\]\s[A-Z]+)([^-]+)", RegexOptions.Singleline);
            var m = rexcode.Match(line);
            return m.Groups[1].Value;
        }
        public static DateTime? GetLogTime(string line)
        {
            Regex rextime = new Regex(@"((\d{4}-\d{2}-\d{2})\s(\d{2}:\d{2}:\d{2}))(,(\d+))*", RegexOptions.Singleline);
            var m = rextime.Match(line);
            if (m.Success)
            {
                return DateTime.Parse(m.Groups[1].Value);
            }
            else
            {
                return null;
            }
        }
        public static List<string> ReadLinesSince(string path, DateTime since)
        {
            List<string> ret = new List<string>();
            string curread = string.Empty;
            // open read only, we don't want any chance of writing data
            using (System.IO.Stream fs = System.IO.File.OpenRead(path))
            {
                // check for empty file
                if (fs.Length == 0)
                {
                    return null;
                }

                // start at end of file
                fs.Position = fs.Length - 1;
                long toPos = fs.Length - 1;

                // the file must end with a '\n' char, if not a partial line write is in progress
                int byteFromFile = fs.ReadByte();
                if (byteFromFile != '\n')
                {
                    // partial line write in progress, do not return the line yet
                    return null;
                }

                // move back to the new line byte - the loop will decrement position again to get to the byte before it
                fs.Position--;

                // while we have not yet reached start of file, read bytes backwards until '\n' byte is hit
                while (fs.Position > 0)
                {
                    fs.Position--;
                    byteFromFile = fs.ReadByte();
                    if (byteFromFile < 0)
                    {
                        // the only way this should happen is if someone truncates the file out from underneath us while we are reading backwards
                        throw new System.IO.IOException("Error reading from file at " + path);
                    }
                    else if (byteFromFile == '\n')
                    {
                        // we found the new line, break out, fs.Position is one after the '\n' char
                        int readThisMuch = (int)(toPos - fs.Position);
                        toPos = fs.Position;
                        byte[] bytes = new System.IO.BinaryReader(fs).ReadBytes(readThisMuch);
                        fs.Position -= readThisMuch;
                        fs.Position--;
                        string line = System.Text.Encoding.UTF8.GetString(bytes);
                        DateTime? dt = GetLogTime(line);
                        if(dt != null)
                        { 
                            if(dt > since)
                            {
                                ret.Insert(0, line.TrimEnd(Environment.NewLine.ToCharArray()));
                                if (curread != string.Empty)
                                    ret[0] += Environment.NewLine + curread;
                                curread = string.Empty;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else if(ret.Count > 0)
                        {
                            curread = line.TrimEnd(Environment.NewLine.ToCharArray()) + Environment.NewLine + curread;
                        }
                    }
                    fs.Position--;
                }

                // fs.Position will be right after the '\n' char or position 0 if no '\n' char
                return ret;
            }
        }
    }
}

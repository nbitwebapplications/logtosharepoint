﻿using Microsoft.SharePoint.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogToSharePoint
{
    public class UploadItem
    {
        public string fpath;
        public string filename;
        public JObject metadata;
        public int itemId;
        public Guid fileGuid;
        public SPListEntityKind kind;
        public string serverRelativeUrl;
        public File fileItem;
        public bool highSecurity;
    }

    public enum SPListEntityKind { File, ListItem };
}

﻿using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Search.Query;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security;
using File = Microsoft.SharePoint.Client.File;


namespace LogToSharePoint
{

    public class SharePointClient
    {
        private ClientContext context;
        private Dictionary<string, string> sharepointToWidenFieldMap = new Dictionary<string, string>();
        private const int C_DEFAULT_QUERY_COUNT = 20;
        private List<string> acceptStatuses = new List<string>();

        public SharePointClient(string apiurl, string suser, string spassword, List<string> acceptFields = null)
        {
            context = new ClientContext(apiurl);
            SecureString passWord = new SecureString();

            foreach (char c in spassword.ToCharArray()) passWord.AppendChar(c);
            context.Credentials = new SharePointOnlineCredentials(suser, passWord);

            if (acceptFields != null)
            {
                acceptStatuses.AddRange(ConfigurationManager.AppSettings["statuses_for_transfer"].Split(new char[] { ',' }));
                foreach (string fld in acceptFields)
                {
                    if (fld.Contains("="))
                    {
                        string[] fldparts = fld.Split("=".ToCharArray());
                        sharepointToWidenFieldMap.Add(fldparts[0], fldparts[1]);
                    }
                    else
                        sharepointToWidenFieldMap.Add(fld, fld);
                }
            }
        }

        public List<UploadItem> GetListItems(string listTitle, int count = -1)
        {
            List<UploadItem> ret = new List<UploadItem>();
            List list = context.Web.Lists.GetByTitle(listTitle);
            List<string> ids = new List<string>();
            ListItemCollection items;
            ListItemCollectionPosition position = null;

            // This creates a CamlQuery that has a RowLimit of 100, and also specifies Scope="RecursiveAll" 
            // so that it grabs all list items, regardless of the folder they are in.

            while (count > 0)
            {
                CamlQuery query = CamlQuery.CreateAllItemsQuery((count > 0 && count < C_DEFAULT_QUERY_COUNT) ? count : C_DEFAULT_QUERY_COUNT);
                query.ListItemCollectionPosition = position;

                items = list.GetItems(query);

                // Retrieve all items in the ListItemCollection from List.GetItems(Query). 
                // https://www.codeproject.com/Tips/1119844/Programatically-Copy-all-SharePoint-List-Items-to
                context.Load(items);
                context.ExecuteQuery();

                if (items.Count == 0)
                    break;

                count -= items.Count;
                position = items.ListItemCollectionPosition;

                foreach (var listItem in items)
                {
                    var oAttachments = listItem.AttachmentFiles;
                    context.Load(oAttachments);
                    context.ExecuteQuery();

                    JObject metadata = GetMetadataFromFieldValues(listItem.FieldValues);

                    foreach (Attachment oAttachment in oAttachments)
                    {
                        File attachmentsFile = context.Web.GetFileByServerRelativeUrl(oAttachment.ServerRelativeUrl);
                        context.Load(attachmentsFile);

                        UploadItem ui = GetUploadItemFromFile(attachmentsFile);

                        ui.metadata = metadata;
                        ui.itemId = listItem.Id;
                        ui.kind = SPListEntityKind.ListItem;
                        ret.Add(ui);
                    }
                }
            }
            return ret;
        }

        private JObject GetMetadataFromFieldValues(Dictionary<string, object> fieldValues)
        {
            var metadata = new JObject();
            metadata.Add("fields", new JObject());
            foreach (var fld in fieldValues)
            {
                //fld.Key
                if (sharepointToWidenFieldMap.ContainsKey(fld.Key))
                {
                    JArray jarr = new JArray();
                    jarr.Add(fld.Value);
                    ((JObject)metadata["fields"]).Add(sharepointToWidenFieldMap[fld.Key], jarr);
                }
            }
            return metadata;
        }
        private UploadItem GetUploadItemFromFile(File attachmentsFile)
        {
            ClientResult<Stream> clientResultStream = attachmentsFile.OpenBinaryStream();
            context.ExecuteQuery();
            var stream = clientResultStream.Value;

            UploadItem ui = new UploadItem();
            ui.fpath = Path.GetTempFileName();
            ui.filename = attachmentsFile.Name;
            ui.fileItem = attachmentsFile;

            FileStream fs = new FileStream(ui.fpath, FileMode.Create);

            byte[] buffer = new byte[1048576]; // 1MB
            int bytesRead = 0;
            int offset = 0;
            do
            {
                bytesRead = stream.Read(buffer, 0, buffer.Length);
                fs.Write(buffer, 0, bytesRead);
                fs.Flush();
                offset += bytesRead;

            } while (bytesRead == buffer.Length);
            fs.Close();
            return ui;
        }

        public List<UploadItem> GetFolderItems(string listTitle, string folderName, int count = -1)
        {
            List<UploadItem> ret = new List<UploadItem>();
            List list = context.Web.Lists.GetByTitle(listTitle);

            context.Load(list.RootFolder);
            context.Load(list.RootFolder.Folders);
            context.ExecuteQuery();
            // This sentence returns the folder searching by its name
            var folder = list.RootFolder.Folders.FirstOrDefault(f => f.Name == folderName);
            context.Load(folder.Files);
            context.ExecuteQuery();
            foreach (var file in folder.Files)
            {
                context.Load(file.ListItemAllFields);
                context.ExecuteQuery();
                if (file.ListItemAllFields["Status"].ToString() == "Ready for Widen")
                {
                    UploadItem ui = GetUploadItemFromFile(file);
                    ui.metadata = GetMetadataFromFieldValues(file.ListItemAllFields.FieldValues);
                    ui.fileGuid = file.UniqueId;
                    ui.kind = SPListEntityKind.File;
                    ui.highSecurity = (bool)file.ListItemAllFields["HighSecurity"];
                    ui.serverRelativeUrl = file.ServerRelativeUrl;
                    ret.Add(ui);
                }
                if (count > 0 && ret.Count == count)
                    break;
            }
            return ret;
        }



        public void DeleteListItem(string listTitle, int itemId)
        {
            List oList = context.Web.Lists.GetByTitle(listTitle);
            ListItem oListItem = oList.GetItemById(itemId);
            //oListItem["Title"] = "Custom Title updated Programmatically.";
            oListItem.DeleteObject();
            context.ExecuteQuery();
        }
        public void UpdateListItem(string listTitle, int itemId)
        {
            List oList = context.Web.Lists.GetByTitle(listTitle);
            ListItem oListItem = oList.GetItemById(itemId);
            oListItem["IsUploaded"] = true;
            oListItem.Update();
            context.ExecuteQuery();
        }

        public void DeleteFolderFile(string listTitle, string relativeUrl)
        {
            var file = context.Web.GetFileByServerRelativeUrl(relativeUrl);
            //var file = folder.Files.GetByUrl(relativeUrl);
            context.Load(file);
            context.ExecuteQuery();
            file.DeleteObject();
            context.ExecuteQuery();
        }

        public void LogAssetStatusItem(JObject ui)
        {
            List aList = context.Web.Lists.GetByTitle("AssetStatus");
            ListItemCreationInformation lici = new ListItemCreationInformation();
            var item = aList.AddItem(lici);
            foreach(var jt in ui)
            {
                item[jt.Key] = ui[jt.Key];
            }
            item.Update();
            context.ExecuteQuery();
        }

        public void CreateListItem(string listName, JObject ui)
        {
            List aList = context.Web.Lists.GetByTitle(listName);
            ListItemCreationInformation lici = new ListItemCreationInformation();
            var item = aList.AddItem(lici);
            foreach (var jt in ui)
            {
                DateTime dt;
                if(DateTime.TryParse(ui[jt.Key].ToString(), out dt))
                {
                    item[jt.Key] = dt;
                }
                else
                {
                    item[jt.Key] = ui[jt.Key].ToString();
                }
            }
            item.Update();
            context.ExecuteQuery();
        }

        public void UpdateProcessStatus(string title, string status, DateTime? recordedAt, DateTime? errorAt)
        {
            List aList = context.Web.Lists.GetByTitle("ProcessStatus");
            CamlQuery camlQuery = new CamlQuery();
            camlQuery.ViewXml = $"<View><Query><Where><Eq><FieldRef Name='Title' /><Value Type='Text'>{title}</Value></Eq></Where></Query></View>";
            ListItemCollection collListItem = aList.GetItems(camlQuery);
            context.Load(collListItem);
            context.ExecuteQuery();
            if (collListItem.Count > 0)
            {
                var update = collListItem[0];
                update["Status"] = status;
                update["LastActive"] = recordedAt;
                if (status == "ERROR" || status == "FATAL")
                {
                    update["LastError"] = errorAt;
                }
                update.Update();
                context.ExecuteQuery();
            }
            else
            {
                JObject item = new JObject();
                item["Title"] = title;
                item["Status"] = status;
                item["LastActive"] = recordedAt;
                if (status == "ERROR" || status == "FATAL")
                {
                    item["LastError"] = recordedAt;
                }
                CreateListItem("ProcessStatus", item);
            }
        }
    }
}

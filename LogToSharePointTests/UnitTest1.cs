﻿using System;
using System.Text.RegularExpressions;
using LogToSharePoint;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LogToSharePointTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ReadLinesSince()
        {
            var lines = HelperFns.ReadLinesSince("Logs/mendix_csv_service.log", DateTime.Now.AddDays(-7));

        }

        [TestMethod]
        public void ReadErrorLines()
        {
            string error = null;
            DateTime? errorTime = new DateTime();

            Regex rexlinecode = new Regex(@"(?>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2},\d+\s\[\d+\]\s)([A-Z]+)", RegexOptions.Singleline);
            Regex rextime = new Regex(@"((\d{4}-\d{2}-\d{2})\s(\d{2}:\d{2}:\d{2}))(,(\d+))*", RegexOptions.Singleline);

            var lines = HelperFns.ReadLinesSince("Logs/syncwidenassets.log", DateTime.Now.AddDays(-7));

            foreach (var line in lines)
            {
                var m = rexlinecode.Match(line);
                if (m.Success)
                {
                    var code = m.Groups[1].Value;
                    if (code == "ERROR")
                    {
                        error = line.Substring(m.Groups[1].Index + code.Length).Trim();
                        errorTime = HelperFns.GetLogTime(line);
                    }
                }
            }
            Console.WriteLine(error);
        }
    }
}

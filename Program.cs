﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Reflection;

namespace LogToSharePoint
{
    class Program
    {
        static void Main(string[] args)
        {
            var since = DateTime.Parse("2021-02-12 13:08:01");
            var username = ConfigurationManager.AppSettings["sharepoint_user_name"];
            var password = ConfigurationManager.AppSettings["sharepoint_user_password"];
            var url = ConfigurationManager.AppSettings["sharepoint_list"];
            var spclient = new SharePointClient(url, username, password);
            var lastrun = "2021-01-01T00:00:00Z";
            Dictionary<string, string> logNameMap = new Dictionary<string, string>();
            List<string> logs = new List<string>();

            var lastrunFilename = "lastrun.txt";
            var fileLastRun = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), lastrunFilename);
            if (File.Exists(fileLastRun))
            {
                lastrun = File.ReadAllText(fileLastRun);
            }

            if (args.Length == 0)
            {
                throw new Exception("Must provide a logfile");
            }

            var logfile = args[0];
            if(!File.Exists(logfile))
            {
                throw new Exception(logfile + " not found");
            }
            var flines = File.ReadAllLines(logfile);
            foreach(var line in flines)
            {
                var lineparts = line.Split("=".ToCharArray());
                var log = line;
                if(lineparts.Length > 0)
                {
                    logNameMap.Add(lineparts[0], lineparts[1]);
                    log = lineparts[0];
                }
                if(File.Exists(log))
                {
                    logs.Add(log);
                }
            }

            foreach(var log in logs)
            {
                List<string> lines;
                try
                {
                    lines = HelperFns.ReadLinesSince(log, DateTime.Parse(lastrun));
                }
                catch (Exception e)
                {
                    var err = new Exception($"Error reading log {log}", e);
                    continue;
                }

                if (lines.Count == 0)
                    continue;

                Regex rexlinecode = new Regex(@"(?>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2},\d+\s\[\d+\]\s)([A-Z]+)", RegexOptions.Singleline);
                Regex rextime = new Regex(@"((\d{4}-\d{2}-\d{2})\s(\d{2}:\d{2}:\d{2}))(,(\d+))*", RegexOptions.Singleline);

                string error = null;
                string fatal = null;
                string procName = HelperFns.GetProcessName(lines[0]);
                if (logNameMap.ContainsKey(log))
                    procName = logNameMap[log];
                DateTime? errorTime = new DateTime();
                DateTime? logTime = new DateTime();
                DateTime? fatalTime = new DateTime();

                if (lines.Count > 0)
                {
                    foreach (var line in lines)
                    {
                        var m = rexlinecode.Match(line);
                        if (m.Success)
                        {
                            var code = m.Groups[1].Value;
                            if (code == "ERROR")
                            {
                                error = line.Substring(m.Groups[1].Index + code.Length).Trim();
                                errorTime = HelperFns.GetLogTime(line);
                            }
                            else if(code == "FATAL")
                            {
                                fatal = line.Substring(m.Groups[1].Index + code.Length).Trim();
                                fatalTime = HelperFns.GetLogTime(line);
                            }
                        }
                        logTime = HelperFns.GetLogTime(line);
                    }
                    if (error != null)
                    {
                        JObject msg = new JObject();

                        msg["Title"] = procName;
                        msg["Status"] = "ERROR";
                        msg["Message"] = error;
                        msg["LogDate"] = errorTime;
                        //msg.Add("LogDate", errorTime);
                        spclient.CreateListItem("Log", msg);
                        spclient.UpdateProcessStatus(procName, "ERROR", logTime, errorTime);
                    }
                    else if(fatal != null)
                    {
                        JObject msg = new JObject();
                        msg["Title"] = procName;
                        msg["Status"] = "FATAL";
                        msg["Message"] = fatal;
                        msg["LogDate"] = fatalTime;
                        //msg.Add("LogDate", errorTime);
                        spclient.CreateListItem("Log", msg);
                        spclient.UpdateProcessStatus(procName, "FATAL", logTime, fatalTime);
                    }
                    else
                    {
                        spclient.UpdateProcessStatus(procName, "OK", logTime, null);
                    }
                }
                else
                {
                    // no update since last run
                }
            }


            StreamWriter sw = new StreamWriter(fileLastRun);
            sw.WriteLine(DateTime.Now.ToString("u"));
            sw.Flush();
            sw.Close();
        }
    }
}
